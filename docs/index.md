# FabricErrorMod

## Introduction
This is a simple mod that adds many OVERPOWERED items (available in survival) to the game.
 \[REQUIRES FABRIC API\]

## Usage
### From Binaries
1. Download the mod from GitHub Actions.
2. Drop the file to your mods folder.

### Build from Source
1. Clone the project.
```
$ git clone https://github.com/UjhhgtgTeams/fabric-error-mod.git
```
2. Build.
```
Windows: gradlew.bat build
Linux: $ chmod +x ./gradlew && ./gradlew build
```
3. Drop the file to your mods folder.

## Navigator
[Back: Main Site](https://ujhhgtgteams.github.io/)